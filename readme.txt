Projet : Gestion de salle AFPA
Auteur : Marine , Alexandra , Guillaume, Ga�tan, Hakim	

Technologies et outils utilis�s : Java / Maven / Lombok / Hibernate / JEE / Spring / Gitkraken / Bitbucket / JMerise / PgAdmin 4 / DBeaver 

D�marche � suivre :

1) Sur pgAdmin, cr�er un r�le de connexion.

	nom = admin 
	mot de passe = 1234 

2) Sur pgAdmin cr�er une nouvelle base de donn�es (nom : gestionSalle) PostgreSQL(avec l'user admin)

3) Executer le script "script_creation_et_insertion.sql"( qui se trouve dans le dossier documentation/BDD)

4) placer le fichier salleafpa.war dans le dossier webapps du serveur TomCat(version 9)

5) Demarrer le serveur TomCat (version 9)
	
6) Fonctionnement de l'application : 

	Acc�der � l'application grace � l'url de cette page est http://localhost:8080/salleafpa/

	Vous pouvez soit vous authentifier pour acc�der � la partie gestion salle ou cliquer sur le lien connexion en tant qu'admistrateur pour acc�der � 
	la pasge d'authentification de l'administrateur.
	Des utilisateurs sont cr��s par d�faut � l'aide du script sql (administrateur(login : adm, mot de passe : admin) et utilisateur login : mb, mot de passe : 1234)

	- Partie gestion salle : 

		- Visualiser les salles et faire des recherches sur les salles

		Si vous avez un role d'administrateur : 
		- Cr�er des salles
		- modifier les salles
		- faire une reservation
		- modifier une r�servation
		- supprimer une reservation
		- cr�er des batiments
		- cr�er des types de mat�riaux
		- cr�er des types de salles
	
	- Partie administrateur - gestion utilisateur : 

		- Cr�er des utilisateur et des administrateurs
		- Consulter les informations des utilisateurs
		- Modifier les information des utilisateurs
		- Supprimer des utilisateurs

	