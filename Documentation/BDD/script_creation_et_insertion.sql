------------------------------------------------------------
--        Script Postgre 
------------------------------------------------------------



------------------------------------------------------------
-- Table: type_salle
------------------------------------------------------------
CREATE TABLE public.type_salle (
	id_type int4 NOT NULL,
	libelle varchar(50) NOT NULL,
	CONSTRAINT type_salle_pkey PRIMARY KEY (id_type)
);


------------------------------------------------------------
-- Table: batiment
------------------------------------------------------------
CREATE TABLE public.batiment (
	id_batiment int4 NOT NULL,
	nom varchar(50) NOT NULL,
	CONSTRAINT batiment_pkey PRIMARY KEY (id_batiment)
);


------------------------------------------------------------
-- Table: salle
------------------------------------------------------------

CREATE TABLE public.salle (
	id_salle int4 NOT NULL,
	actif bool NOT NULL,
	capacite int4 NOT NULL,
	etage int4 NOT NULL,
	nom varchar(50) NOT NULL,
	numero int4 NOT NULL,
	surface float4 NOT NULL,
	id_batiment int4 NULL,
	id_type int4 NULL,
	CONSTRAINT salle_pkey PRIMARY KEY (id_salle)
	);


------------------------------------------------------------
-- Table: type_materiel
------------------------------------------------------------
CREATE TABLE public.type_materiel (
	id_type_materiel int4 NOT NULL,
	libelle varchar(50) NOT NULL,
	CONSTRAINT type_materiel_pkey PRIMARY KEY (id_type_materiel)
);


------------------------------------------------------------
-- Table: reservation
------------------------------------------------------------
CREATE TABLE public.reservation (
	id_reservation int4 NOT NULL,
	date_debut date NOT NULL,
	date_fin date NOT NULL,
	nom_reservation varchar(50) NOT NULL,
	id_salle int4 NULL,
	CONSTRAINT reservation_pkey PRIMARY KEY (id_reservation)
	);


------------------------------------------------------------
-- Table: role
------------------------------------------------------------
CREATE TABLE public.role_personne (
	id_role int4 NOT NULL,
	libelle varchar(50) NOT NULL,
	CONSTRAINT role_personne_pkey PRIMARY KEY (id_role)
);



------------------------------------------------------------
-- Table: fonction
------------------------------------------------------------
CREATE TABLE public.fonction (
	id_fonction int4 NOT NULL,
	libelle varchar(50) NOT NULL,
	CONSTRAINT fonction_pkey PRIMARY KEY (id_fonction)
);


------------------------------------------------------------
-- Table: personne_archive
------------------------------------------------------------
CREATE TABLE public.personne_archive(
	id_personne      SERIAL NOT NULL ,
	nom              VARCHAR (50) NOT NULL ,
	prenom           VARCHAR (50) NOT NULL ,
	mail             VARCHAR (50) NOT NULL ,
	tel              VARCHAR (50) NOT NULL ,
	adresse          VARCHAR (50) NOT NULL ,
	date_naissance   DATE   ,
	actif            BOOL   ,
	fonction         VARCHAR (50)  ,
	date_archive     DATE  NOT NULL  ,
	CONSTRAINT personne_archive_PKEY PRIMARY KEY (id_personne)
)WITHOUT OIDS;

------------------------------------------------------------
-- Table: reservation_archive_sup
------------------------------------------------------------
CREATE TABLE public.reservation_archive_sup (
	id_reservation int4 NOT NULL,
	date_debut date NOT NULL,
	date_fin date NOT NULL,
	nom_reservation varchar(50) NOT NULL,
	id_salle int4 NULL,
	date_archive     DATE  NOT NULL  ,
	CONSTRAINT reservation_archive_sup_pkey PRIMARY KEY (id_reservation)
	);



------------------------------------------------------------
-- Table: materiel
------------------------------------------------------------
CREATE TABLE public.materiel (
	id_materiel int4 NOT NULL,
	quantite int4 NOT NULL,
	id_salle int4 NULL,
	id_type_materiel int4 NULL,
	CONSTRAINT materiel_pkey PRIMARY KEY (id_materiel)
	);

------------------------------------------------------------
-- Table: personne
------------------------------------------------------------
CREATE TABLE public.personne (
	id_personne int4 NOT NULL,
	actif bool NOT NULL,
	adresse varchar(50) NOT NULL,
	date_naissance date NOT NULL,
	mail varchar(50) NOT NULL,
	nom varchar(50) NOT NULL,
	prenom varchar(50) NOT NULL,
	tel varchar(50) NOT NULL,
	login varchar(50) NULL,
	id_fonction int4 NULL,
	id_role int4 NULL,
	CONSTRAINT personne_pkey PRIMARY KEY (id_personne)
	);


------------------------------------------------------------
-- Table: authentification
------------------------------------------------------------
CREATE TABLE public.authentification (
	login varchar(50) NOT NULL,
	mdp varchar(50) NOT NULL,
	CONSTRAINT authentification_pkey PRIMARY KEY (login)
);

------------------------------------------------------------
-- Constraintes cl� etrang�res
------------------------------------------------------------


ALTER TABLE public.salle
	ADD CONSTRAINT salle_type_salle0_FK
	FOREIGN KEY (id_type)
	REFERENCES public.type_salle(id_type);

ALTER TABLE public.salle
	ADD CONSTRAINT salle_batiment1_FK
	FOREIGN KEY (id_batiment)
	REFERENCES public.batiment(id_batiment);

ALTER TABLE public.reservation
	ADD CONSTRAINT reservation_salle0_FK
	FOREIGN KEY (id_salle) 
	REFERENCES salle(id_salle);

ALTER TABLE public.personne
	ADD CONSTRAINT personne_authentificaion0_FK
	FOREIGN KEY (login)
	REFERENCES public.authentification(login);

ALTER TABLE public.personne
	ADD CONSTRAINT personne_role0_FK
	FOREIGN KEY (id_role)
	REFERENCES public.role_personne(id_role);

ALTER TABLE public.personne
	ADD CONSTRAINT personne_fonction0_FK
	FOREIGN KEY (id_fonction)
	REFERENCES public.fonction(id_fonction);

ALTER TABLE public.materiel 
	ADD CONSTRAINT materiel_type0_FK
	FOREIGN KEY (id_type_materiel) 
	REFERENCES type_materiel(id_type_materiel);

ALTER TABLE public.materiel 
	ADD CONSTRAINT materiel_salle0_FK
	FOREIGN KEY (id_salle) 
	REFERENCES salle(id_salle);


------------------------------------------------------------
-- contraintes format donn�es
------------------------------------------------------------
ALTER TABLE public.personne
	ADD CONSTRAINT personne_date_naissance
	CHECK (cast (to_char(date_naissance,'yyyy') as integer)
	between cast (to_char(now(),'yyyy') as integer)-88 
and cast (to_char(now(),'yyyy') as integer)-16);

ALTER TABLE public.personne
	ADD CONSTRAINT personne_desactivation_admin
	CHECK ((id_role=1 and actif=true) or id_role!=1);


ALTER TABLE public.personne
	ADD CONSTRAINT personne_unique_login
	UNIQUE (login);

ALTER TABLE public.personne
	ADD CONSTRAINT personne_tel
	CHECK (tel ~ E'^\\d{10}$');

ALTER TABLE public.personne
	ADD CONSTRAINT personne_prenom
	CHECK (prenom ~ E'^[A-Za-z \\-���]+$');

ALTER TABLE public.personne
	ADD CONSTRAINT personne_nom
	CHECK (nom ~ E'^[A-Za-z \\-���]+$');

ALTER TABLE public.reservation
	ADD CONSTRAINT date_reservation
	CHECK (date_debut<=date_fin);


------------------------------------------------------------
-- creation sequence
------------------------------------------------------------

Create sequence seq_personne owned by personne.id_personne;
Create sequence seq_role owned by role_personne.id_role;
Create sequence seq_fonction owned by fonction.id_fonction;
Create sequence seq_batiment owned by batiment.id_batiment;
Create sequence seq_type_salle owned by type_salle.id_type;
Create sequence seq_salle owned by salle.id_salle;
Create sequence seq_type_materiel owned by type_materiel.id_type_materiel;
Create sequence seq_reservation owned by reservation.id_reservation;
Create sequence seq_materiel owned by materiel.id_materiel;



------------------------------------------------------------
-- trigger suppression personne
------------------------------------------------------------

create or replace function delete_utilisateur() returns trigger as
$body$
declare 
fonction varchar;
begin
	select libelle into fonction from fonction f where old.id_fonction=f.id_fonction;
	if(old.id_role=1) then
		raise exception 'un administrateur ne peux pas �tre desactiver';
	else
		INSERT INTO public.personne_archive
			(nom, prenom, mail, tel, adresse, date_naissance, actif, fonction, date_archive)
				VALUES(old.nom, old.prenom, old.mail, old.tel, old.adresse, old.date_naissance, old.actif, fonction, now());
		return old;
	end if;
end
$body$
language plpgsql;

create trigger trigger_delete_utilisateur
before delete on personne
for each row execute procedure delete_utilisateur();


------------------------------------------------------------
-- trigger suppression reservation
------------------------------------------------------------

create or replace function delete_reservation() returns trigger as
$body$
declare 
fonction varchar;
begin
	
	if(old.date_debut<=now()) then
		raise exception 'une reservation en cours ne peux pas �tre supprimer';
	else
		INSERT INTO public.reservation_archive_sup
			(id_reservation, date_debut, date_fin, nom_reservation, id_salle, date_archive)
				VALUES(old.id_reservation,old.date_debut, old.date_fin, old.nom_reservation, old.id_salle, now());

		return old;
	end if;
end
$body$
language plpgsql;

create trigger trigger_delete_utilisateur
before delete on reservation
for each row execute procedure delete_reservation();

------------------------------------------------------------
-- insertion donn�es
------------------------------------------------------------

insert into role_personne values (nextval('seq_role'),'ADMINISTRATEUR');
insert into role_personne values (nextval('seq_role'),'UTILISATEUR');

insert into fonction values (nextval('seq_fonction'),'DIRECTEUR');
insert into fonction values (nextval('seq_fonction'),'FORMATEUR');
insert into fonction values (nextval('seq_fonction'),'STAGIAIRE');
insert into fonction values (nextval('seq_fonction'),'SECRETAIRE');
insert into fonction values (nextval('seq_fonction'),'VISITEUR');
insert into fonction values (nextval('seq_fonction'),'INTERVENANT');


insert into authentification values ('adm','admin');
insert into authentification values ('mb','1234');
insert into authentification values ('az','1234');
insert into authentification values ('gg','1234');
insert into authentification values ('mz','1234');
insert into authentification values ('ff','1234');

INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1952-05-01', 'admi@gmail.com', 'superadmin', 'superadmin', '0320101010', 'adm', 1, 1);

INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1984-05-01', 'mb@gmail.com', 'Benjira', 'Mohammed', '0320101010', 'mb', 2, 2);
INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1990-05-01', 'az@gmail.com', 'Zenina', 'Alexandra', '0320101010', 'az', 3, 1);

INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1990-05-01', 'gg@gmail.com', 'Grauwin', 'Guillaume', '0320101010', 'gg', 4, 1);

INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1990-05-01', 'gg@gmail.com', 'Zimmer', 'Marine', '0320101010', 'mz', 6, 1);

INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1990-05-01', 'gg@gmail.com', 'Fayak', 'Fayaz', '0320101010', 'ff', 5, 2);
INSERT INTO public.batiment (id_batiment, nom) VALUES(nextval('seq_batiment'), 'A');
INSERT INTO public.batiment (id_batiment, nom) VALUES(nextval('seq_batiment'), 'B');

INSERT INTO public.type_salle (id_type, libelle) VALUES(nextval('seq_type_salle'), 'Salle de cours');
INSERT INTO public.type_salle (id_type, libelle) VALUES(nextval('seq_type_salle'), 'Bureau');

INSERT INTO public.salle (id_salle, actif, capacite, etage, nom, numero, surface, id_batiment, id_type)
VALUES(nextval('seq_salle'), true, 20, 0, 'CDA', 1, 60, 1, 1);

INSERT INTO public.salle (id_salle, actif, capacite, etage, nom, numero, surface, id_batiment, id_type)
VALUES(nextval('seq_salle'), true, 5, 6, 'FAYAK', 61, 10, 2, 2);

INSERT INTO public.salle (id_salle, actif, capacite, etage, nom, numero, surface, id_batiment, id_type)
VALUES(nextval('seq_salle'), true, 50, 2, 'SSS', 21, 100, 1, 1);

INSERT INTO public.salle (id_salle, actif, capacite, etage, nom, numero, surface, id_batiment, id_type)
VALUES(nextval('seq_salle'), true, 40, 0, 'CHAT', 2, 40, 1, 1);

INSERT INTO public.type_materiel (id_type_materiel, libelle)
VALUES(nextval('seq_type_materiel'), 'Chaise');

INSERT INTO public.type_materiel (id_type_materiel, libelle)
VALUES(nextval('seq_type_materiel'), 'Ordinateur');

INSERT INTO public.type_materiel (id_type_materiel, libelle)
VALUES(nextval('seq_type_materiel'), 'Tableau');


INSERT INTO public.reservation (id_reservation, date_debut, date_fin, nom_reservation, id_salle)
VALUES(nextval('seq_reservation'), '2020-01-01', '2020-03-03', 'cda-158', 1);

INSERT INTO public.reservation (id_reservation, date_debut, date_fin, nom_reservation, id_salle)
VALUES(nextval('seq_reservation'), '2020-01-01', '2020-03-03', 'glandeur', 2);

INSERT INTO public.reservation (id_reservation, date_debut, date_fin, nom_reservation, id_salle)
VALUES(nextval('seq_reservation'), '2020-01-01', '2020-01-02', 'dodo', 3);


INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 15, 1, 1);
INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 15, 1, 2);
INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 2, 1, 3);

INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 5, 2, 1);
INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 5, 2, 2);
INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 1, 2, 3);

INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 35, 3, 1);
INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 35, 3, 2);
INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 3, 3, 3);





