insert into role_personne values (nextval('seq_role'),'ADMINISTRATEUR');
insert into role_personne values (nextval('seq_role'),'UTILISATEUR');

insert into fonction values (nextval('seq_fonction'),'DIRECTEUR');
insert into fonction values (nextval('seq_fonction'),'FORMATEUR');
insert into fonction values (nextval('seq_fonction'),'STAGIAIRE');
insert into fonction values (nextval('seq_fonction'),'SECRETAIRE');
insert into fonction values (nextval('seq_fonction'),'VISITEUR');
insert into fonction values (nextval('seq_fonction'),'INTERVENANT');


insert into authentification values ('adm','admin');
insert into authentification values ('mb','1234');
insert into authentification values ('az','1234');
insert into authentification values ('gg','1234');
insert into authentification values ('mz','1234');
insert into authentification values ('ff','1234');

INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1952-05-01', 'admi@gmail.com', 'superadmin', 'superadmin', '0320101010', 'adm', 1, 1);

INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1984-05-01', 'mb@gmail.com', 'Benjira', 'Mohammed', '0320101010', 'mb', 2, 2);
INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1990-05-01', 'az@gmail.com', 'Zenina', 'Alexandra', '0320101010', 'az', 3, 1);

INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1990-05-01', 'gg@gmail.com', 'Grauwin', 'Guillaume', '0320101010', 'gg', 4, 1);

INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1990-05-01', 'gg@gmail.com', 'Zimmer', 'Marine', '0320101010', 'mz', 6, 1);

INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1990-05-01', 'gg@gmail.com', 'Fayak', 'Fayaz', '0320101010', 'ff', 5, 2);
INSERT INTO public.batiment (id_batiment, nom) VALUES(nextval('seq_batiment'), 'A');
INSERT INTO public.batiment (id_batiment, nom) VALUES(nextval('seq_batiment'), 'B');

INSERT INTO public.type_salle (id_type, libelle) VALUES(nextval('seq_type_salle'), 'Salle de cours');
INSERT INTO public.type_salle (id_type, libelle) VALUES(nextval('seq_type_salle'), 'Bureau');

INSERT INTO public.salle (id_salle, actif, capacite, etage, nom, numero, surface, id_batiment, id_type)
VALUES(nextval('seq_salle'), true, 20, 0, 'CDA', 1, 60, 1, 1);

INSERT INTO public.salle (id_salle, actif, capacite, etage, nom, numero, surface, id_batiment, id_type)
VALUES(nextval('seq_salle'), true, 5, 6, 'FAYAK', 61, 10, 2, 2);

INSERT INTO public.salle (id_salle, actif, capacite, etage, nom, numero, surface, id_batiment, id_type)
VALUES(nextval('seq_salle'), true, 50, 2, 'SSS', 21, 100, 1, 1);

INSERT INTO public.salle (id_salle, actif, capacite, etage, nom, numero, surface, id_batiment, id_type)
VALUES(nextval('seq_salle'), true, 40, 0, 'CHAT', 2, 40, 1, 1);

INSERT INTO public.type_materiel (id_type_materiel, libelle)
VALUES(nextval('seq_type_materiel'), 'Chaise');

INSERT INTO public.type_materiel (id_type_materiel, libelle)
VALUES(nextval('seq_type_materiel'), 'Ordinateur');

INSERT INTO public.type_materiel (id_type_materiel, libelle)
VALUES(nextval('seq_type_materiel'), 'Tableau');


INSERT INTO public.reservation (id_reservation, date_debut, date_fin, nom_reservation, id_salle)
VALUES(nextval('seq_reservation'), '2020-01-01', '2020-03-03', 'cda-158', 1);

INSERT INTO public.reservation (id_reservation, date_debut, date_fin, nom_reservation, id_salle)
VALUES(nextval('seq_reservation'), '2020-01-01', '2020-03-03', 'glandeur', 2);

INSERT INTO public.reservation (id_reservation, date_debut, date_fin, nom_reservation, id_salle)
VALUES(nextval('seq_reservation'), '2020-01-01', '2020-01-02', 'dodo', 3);


INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 15, 1, 1);
INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 15, 1, 2);
INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 2, 1, 3);

INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 5, 2, 1);
INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 5, 2, 2);
INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 1, 2, 3);

INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 35, 3, 1);
INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 35, 3, 2);
INSERT INTO public.materiel (id_materiel, quantite, id_salle, id_type_materiel)
VALUES(nextval('seq_materiel'), 3, 3, 3);
