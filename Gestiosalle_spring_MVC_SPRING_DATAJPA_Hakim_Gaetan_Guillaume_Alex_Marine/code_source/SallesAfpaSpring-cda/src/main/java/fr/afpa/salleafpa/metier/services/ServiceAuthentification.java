package fr.afpa.salleafpa.metier.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dto.iservices.IServiceAuthentificationDto;
import fr.afpa.salleafpa.metier.entities.Personne;
import fr.afpa.salleafpa.metier.iservices.IServiceAuthentification;

@Service
public class ServiceAuthentification implements IServiceAuthentification{
	
	@Autowired
	private IServiceAuthentificationDto servAuthDto; 

	@Override
	public Personne authentification(String login, String mdp) {
		return servAuthDto.authentification(login,mdp);
	}

}
