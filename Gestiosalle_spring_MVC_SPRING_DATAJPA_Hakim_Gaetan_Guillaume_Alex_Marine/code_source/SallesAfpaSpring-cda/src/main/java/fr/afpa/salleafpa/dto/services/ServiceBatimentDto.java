package fr.afpa.salleafpa.dto.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dao.entities.BatimentDao;
import fr.afpa.salleafpa.dao.entities.SalleDao;
import fr.afpa.salleafpa.dao.repositories.BatimentRepository;
import fr.afpa.salleafpa.dto.iservices.IServiceBatimentDto;
import fr.afpa.salleafpa.metier.entities.Batiment;

@Service
public class ServiceBatimentDto implements IServiceBatimentDto {

	@Autowired
	private BatimentRepository batimentDaoRepo;

	@Override
	public List<Batiment> getAll() {

		List<BatimentDao> listeBatimentDao = batimentDaoRepo.findAll();
		List<Batiment> listeBatiment = listeBatimentDao.stream().map(ServiceBatimentDto::batimentDaoToBatiment)
				.collect(Collectors.toList());
		return listeBatiment;

	}
	public static Batiment batimentDaoToBatiment(BatimentDao batimentDao) {
		Batiment batiment = new Batiment();
		batiment.setId(batimentDao.getId());
		batiment.setNom(batimentDao.getNom());
		return batiment;

	}
	public static BatimentDao batimentToBatimentDao(Batiment batiment) {
		BatimentDao batimentDao = new BatimentDao();
		batimentDao.setId(batiment.getId());
		batimentDao.setNom(batiment.getNom());
		List<SalleDao> listeSalleDao = new ArrayList<SalleDao>();
		batimentDao.setListeSalle(listeSalleDao);

		return batimentDao;

	}

	@Override
	public boolean saveBatiment(Batiment batiment) {
		try {
			BatimentDao batimentDao = batimentToBatimentDao(batiment);

			batimentDaoRepo.save(batimentDao);
			return true;
		}

		catch (Exception e) {
			Logger.getLogger(ServiceBatimentDto.class.getName()).log(Level.SEVERE, null, e);
			return false;
		}

	}

	@Override
	public boolean modifBatiment(Batiment batiment) {
		try {
			BatimentDao batimentDao = batimentToBatimentDao(batiment);

			batimentDao.setNom(batiment.getNom());

			batimentDaoRepo.save(batimentDao);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public Batiment getOneBatiment(int id) {
		Optional<BatimentDao> batimentDao = batimentDaoRepo.findById(id);

		return batimentDaoToBatiment(batimentDao.get());
	}
}