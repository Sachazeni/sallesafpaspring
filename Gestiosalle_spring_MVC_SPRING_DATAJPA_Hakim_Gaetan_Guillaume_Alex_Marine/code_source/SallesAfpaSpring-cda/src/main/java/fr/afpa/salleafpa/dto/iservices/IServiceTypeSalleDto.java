package fr.afpa.salleafpa.dto.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.TypeSalle;

public interface IServiceTypeSalleDto {

/**
 * Methode pour retourner une liste de types de salles
 * @return
 */
public List<TypeSalle> getAll();

public TypeSalle getOneTypeSalle(int id);

}
