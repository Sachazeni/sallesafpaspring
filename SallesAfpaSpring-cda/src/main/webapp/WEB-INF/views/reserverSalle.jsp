<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="fr.afpa.salleafpa.outils.Parametrage"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link href="${pageContext.request.contextPath}/resources/css/menu.css" rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/cssVisualisation.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Reservation</title>
</head>
<body>

<body>
	<c:choose>
		<c:when test="${not empty sessionScope.persAuthSalle  }">
			<jsp:include page="menu.jsp" />
			<div class="container">
				<div class="colonne">
				
						<table class="colonne">
						<h2>Choix salle</h2>
						<label>${message }</label>
							<c:forEach var="salle" items="${listeSalle }" varStatus="status">
								<c:if test="${ status.index % 4 == 0}">
									<tr>
								</c:if>
								<td><a href="${Parametrage.URI_RESERVER_UNE_SALLE }/${salle.idSalle}/${dateDebut}/${dateFin}/${nom}">${ salle.nom } <br> Batiment : ${ salle.batiment.nom },
										n� ${ salle.numero }<br> type : ${ salle.typeSalle.libelle }<br>
										etage ${ salle.etage }<br> capacit� : ${ salle.capacite }
								</a></td>
								<c:if test="${status.index+1 % 4 == 0 || status.last}">
									</tr>
								</c:if>
							</c:forEach>

						</table>
				</div>
				
				<div class="colonne2">
				<h2>Formulaire r�servation</h2>
				<form method="post"  action="${ Parametrage.URI_RESERVER_SALLE}">
				
				<!-- filtre date -->
				
				<div>
				<label >Date debut : </label>
				<input id="dateDebut" name="dateDebut" type="date" required="" value="${dateDebut }">
				<br>
				<label >Date fin : </label>
				<input id="dateFin" name="dateFin" type="date" required="" value="${dateFin }">
				<br>
				<em class="erreur">${dateKO}</em>
				<em class="erreur">${occupeKO}</em>
				
				<br>
				
					<!--Nom -->
				<label >Nom r�servation : </label>
				<input id="nom" name="nom" type="text" value="${nom }">
				<br>
				<em class="erreur">${nomKO}</em>
				<br>
				
				
					<!-- Capacite min -->
				<label >Capacit� min : </label>
				<input id="capacite" name="capacite" type="number" value="${capacite }">
				<br>
				 <br>
				<!-- filtre type salle -->
					<input type="text" value="0" name="typeSalle" hidden="">
					  <label>Type de salle : </label>
					  <br>
				<c:forEach var="typeSalle" items="${listeTypeSalle }" varStatus="status">
				<input type="checkbox" value="${typeSalle.id }" name="typeSalle">
                              <label>${typeSalle.libelle }</label>
                              <c:if test="${status.index+1 % 3 == 0 }">
									<br>
								</c:if>
                              </c:forEach>
                        <br>
                         <br>
                        <label>Materiel : </label>
					  <br>
                         <!--filtre type materiel-->
                        <c:forEach var= "typeMateriel" items="${listeTypeMateriel }">
                       
                            <label>${typeMateriel.libelle } :</label>
                             <input id ="${typeMateriel.idTypeMateriel}" name="tm${typeMateriel.idTypeMateriel}" value="" type="number" size="3">
                          <br>
                           </c:forEach>
                           
				<br>
				
				<input type="submit" value="Recherche">
				</div>
				</form>
				</div>
					
		</c:when>
		<c:otherwise>
			<c:redirect url=""></c:redirect>
		</c:otherwise>
	</c:choose>

</body>
</html>