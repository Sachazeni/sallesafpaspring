package fr.afpa.salleafpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dto.iservices.IServiceTypeMaterielDto;
import fr.afpa.salleafpa.metier.entities.TypeMateriel;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeMateriel;
@Service
public class ServiceTypeMateriel implements IServiceTypeMateriel{
	@Autowired
	private IServiceTypeMaterielDto serviceTypeMaterielDto;
	
	@Override
	public List<TypeMateriel> getAll() {
		return serviceTypeMaterielDto.getAll();
	}

	@Override
	public boolean saveTypeMateriel(String nom) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean modifTypeMateriel(String nom) {
		// TODO Auto-generated method stub
		return false;
	}

}
