package fr.afpa.salleafpa.metier.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.Batiment;

public interface IServiceBatiment {
	
	public List<Batiment> getAll();
	public boolean saveBatiment(String nom);
	public boolean modifBatiment(String nom);
	public Batiment getOneBatiment(int id);

}
