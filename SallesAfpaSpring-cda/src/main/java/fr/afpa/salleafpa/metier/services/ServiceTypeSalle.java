package fr.afpa.salleafpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dto.iservices.IServiceTypeSalleDto;
import fr.afpa.salleafpa.metier.entities.TypeSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeSalle;

@Service
public class ServiceTypeSalle implements IServiceTypeSalle {
	@Autowired
	private IServiceTypeSalleDto servTypSalDto;
	
	@Override
	public List<TypeSalle> getAll() {
		return servTypSalDto.getAll();
	}

	@Override
	public TypeSalle getOneTypeSalle(int id) {
		return servTypSalDto.getOneTypeSalle(id);
	}
	


}
