package fr.afpa.salleafpa.dto.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dao.entities.SalleDao;
import fr.afpa.salleafpa.dao.entities.TypeSalleDao;
import fr.afpa.salleafpa.dao.repositories.ServiceTypeSalleRepository;
import fr.afpa.salleafpa.dto.iservices.IServiceTypeSalleDto;
import fr.afpa.salleafpa.metier.entities.TypeSalle;

@Service
public class ServiceTypeSalleDto implements IServiceTypeSalleDto {

	@Autowired
	private ServiceTypeSalleRepository servTypeRep;

	/**
	 * Methode pour recuperer une liste de Types de Salles
	 * @return: la liste des types de salles recuperés dans la BDD
	 */
	@Override 
	public List<TypeSalle> getAll() {
		List<TypeSalleDao> listeTypeSalleDao = servTypeRep.findAll();
		List<TypeSalle> listeTypeSalle = listeTypeSalleDao.stream().map(ServiceTypeSalleDto::typeSalleDaoToTypeSalle)
				.collect(Collectors.toList());
		return listeTypeSalle;
	}

	/**
	 * Methode pour transformer l'entite TypeSalle DAO vers l'entite Type
	 * @param typeSalleDao
	 * @return
	 */
	public static TypeSalle typeSalleDaoToTypeSalle(TypeSalleDao typeSalleDao) {
		return new TypeSalle(typeSalleDao.getId(), typeSalleDao.getLibelle());

	}

	/**
	 * Methode pour transformer l'entite TypeSalle metier vers TypeSalle DAO
	 * @param typeSalle
	 * @return
	 */
	public static TypeSalleDao typeSalleToTypeSalleDao(TypeSalle typeSalle) {
		return new TypeSalleDao(typeSalle.getId(), typeSalle.getLibelle(), new ArrayList<SalleDao>());

	}

	/**
	 * Methode pour recuperer le type salle via son Id
	 */
	@Override
	public TypeSalle getOneTypeSalle(int id) {
		Optional<TypeSalleDao> typeSalleDao=servTypeRep.findById(id);

		return typeSalleDaoToTypeSalle(typeSalleDao.get());
	}
	 


}
