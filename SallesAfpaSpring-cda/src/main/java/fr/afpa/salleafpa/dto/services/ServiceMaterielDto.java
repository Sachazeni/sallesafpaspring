package fr.afpa.salleafpa.dto.services;

import fr.afpa.salleafpa.dao.entities.MaterielDao;
import fr.afpa.salleafpa.metier.entities.Materiel;

public class ServiceMaterielDto {

	/**
	 * service de mapping entité MaterielDAO/entité métier Materiel
	 * @param reservationDao entité MaterielDAO à transformé
	 * @return l'entité Materiel métier 
	 */
	public static Materiel materielDaoToMateriel(MaterielDao materielDao) {
		Materiel materiel = new Materiel(materielDao.getId(), materielDao.getQuantite());
		materiel.setTypeMateriel(
				ServiceTypeMaterielDto.typeMaterielDaoToTypeMateriel(materielDao.getTypeMaterielDao()));

		return materiel;
	}

	/**
	 * service de mapping  entité métier Materiel/MaterielDAO
	 * @param reservationDao entité métier Materiel à transformé
	 * @return l'entité MaterielDao
	 */
	public static MaterielDao materielToMaterielDao(Materiel materiel) {
		MaterielDao materielDao = new MaterielDao(materiel.getId(), materiel.getQuantite());
		materielDao
				.setTypeMaterielDao(ServiceTypeMaterielDto.typeMaterielToTypeMaterielDao(materiel.getTypeMateriel()));
		return materielDao;
	}

}
