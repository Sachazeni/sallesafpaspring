package fr.afpa.salleafpa.dto.iservices;

import fr.afpa.salleafpa.metier.entities.Authentification;
import fr.afpa.salleafpa.metier.entities.Personne;

public interface IServiceAuthentificationDto {
	
	/**
	 * service authentification Dto, transforme l'entité Authentification en
	 * parametre en entité AuthentificationDAO, ensuite appel le service DAO
	 * authentification pour vérifier que l'authentification est correcte si le
	 * service DAO renvoie une AuthentificationDAO, il transforme la personne
	 * correspondant à l'authentification en entité personne et la renvoie
	 * 
	 * @param auth : authentification metier à controler
	 * @return une entité personne metier si l'authentification est correcte, null sinon
	 */
	public Personne authentification(String login, String mdp) ;
	
}
